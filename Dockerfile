FROM python:2.7-alpine

WORKDIR /api

RUN apk add --update \
    python \
    python-dev \
    py-pip \
    build-base \
    libffi \
    libffi-dev \
    openssl \
    openssl-dev \
  && rm -rf /var/cache/apk/* \
  && pip install --upgrade pip \
  && pip install virtualenv

COPY . /api
RUN virtualenv venv && venv/bin/pip install -r requirements.txt

EXPOSE 5000
CMD ["venv/bin/python","manager.py","runserver","--host","0.0.0.0"]