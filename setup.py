""" This setup file for RESTful api.
"""

from setuptools import setup, find_packages

setup(
    name='api',
    version='1.0',
    packages=find_packages(),
    url='https://bitbucket.org/Viacheslav_Kudinov/api/src',
    license='Apache 2.0',
    author='Viacheslav Kudinov',
    author_email='viacheslav.kudinov@gmail.com',
    description='A simple RESTful API',
    test_suite='tests'
)
