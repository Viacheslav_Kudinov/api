"""  Unittests of auth_utils module
"""

import unittest
from unittest import TestCase
from api.authentication import auth_utils as au

class TestAuthUtils(unittest.TestCase):
    """ This it main test class.
    """

    def test_get_user_role(self):
        """ Test getting role for admin user
        """
        expected_role = 'admin'
        username = 'nedstark@doe.john'
        role = au.get_user_role(username)
        self.assertEqual(role, expected_role)

    def test_hash_password(self):
    	""" Test getting hash of password for user
    	"""
    	expected_hash = 'b68c937b4778dba803bc45da3d1536765bffad64c01bcf179521f7808bc52e0e'
    	username = 'kaimelannister@doe.john'
    	password = 'password'
    	hashed_password = au.hash_password(username, password)
    	self.assertEqual(hashed_password, expected_hash)

    @unittest.skip('The requires_roles action has not been implemented')
    def test_requires_roles(self):
        """ Test of requires_roles
        """
        pass