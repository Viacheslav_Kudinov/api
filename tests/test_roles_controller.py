"""  Unittests of authentication module
"""
import unittest
from base64 import b64encode
from unittest import TestCase
import api.global_data_sets
from api.api import create_app
from api.controllers import rl_c

class TestRoles(unittest.TestCase):
    """ This it main test class.
    """
    def setUp(self):
        """ Prepare the test fixture
        """
        api = create_app()
        api.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
        self.app = api.test_client()
        self.app.testing = True
        self.app.debug = True
        username='nedstark@doe.john'
        password='password'
        self.headers = {'Authorization': 'Basic %s' % b64encode(bytes(username+':'+password))}

    def tearDown(self):
        """ Reinit data for nex test
        """
        reload(api.global_data_sets)

    def test_get_roles(self):
    	""" Test getting of all the Roles
    	"""
    	result = self.app.get('/roles/', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"roles":', result.data.strip('\n'))

    def test_get_role(self):
        """ Test getting of specific Roles
        """
        result = self.app.get('/roles/2', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"role":', result.data.strip('\n'))

    def test_add_role(self):
        """ Test adding a new Role
        """
        self.headers['Content-Type'] = 'application/json'
        result = self.app.post('/roles/add', data='{"title":"PM", "description": "Can do nothing"}',headers=self.headers)
        self.assertEqual(result.status_code, 201)
        self.assertIn('Can do nothing', result.data.strip('\n'))

    def test_get_role_by_id(self):
        """ Test getting Role by role_id
        """
        role_dict = rl_c.get_role_by_id(1, api.global_data_sets.roles)
        self.assertEqual(1,role_dict[0]['id'])

    def test_get_role_by_title(self):
        """ Test getting Role by title of Role
        """
        role_dict = rl_c.get_role_by_title('automation', api.global_data_sets.roles)
        self.assertEqual('automation',role_dict[0]['title'])
