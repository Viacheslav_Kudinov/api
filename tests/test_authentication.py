"""  Unittests of authentication module
"""

import unittest
from unittest import TestCase
from base64 import b64encode
from api.authentication import authentication as a
from api.api import create_app

class TestAuthentication(unittest.TestCase):
    """ This it main test class.
    """
    
    def setUp(self):
        """ Prepare the test fixture
        """
        api = create_app()
        api.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
        self.app = api.test_client()
        self.app.testing = True
        self.app.debug = True
        username='nedstark@doe.john'
        password='password'
        self.headers = {'Authorization': 'Basic %s' % b64encode(bytes(username+':'+password))}

    def test_get_password(self):
    	""" Test getting of password (callback function)
    	"""
    	expected_password = 'b68c937b4778dba803bc45da3d1536765bffad64c01bcf179521f7808bc52e0e'
    	username = 'kaimelannister@doe.john'
    	password = a.get_password(username)
    	self.assertEqual(expected_password, password)

    def test_hash_pw(self):
    	""" Test getting hash of user password (callback function)
    	"""
    	expected_hash = 'b68c937b4778dba803bc45da3d1536765bffad64c01bcf179521f7808bc52e0e'
    	username = 'kaimelannister@doe.john'
    	password = 'password'
    	hashed_password = a.hash_pw(username, password)
    	self.assertEqual(hashed_password, expected_hash)

    def test_unauthorized(self):
    	""" Test unauthorized mode for secured URL
    	"""
    	result = self.app.get('/users/')
        self.assertEqual(result.status_code, 401)
