"""  Unittests of authentication module
"""
import unittest
from base64 import b64encode
from unittest import TestCase
import api.global_data_sets
from api.api import create_app
from api.controllers import srv_c

class TestServices(unittest.TestCase):
    """ This it main test class
    """
    def setUp(self):
        """ Prepare the test fixture
        """
        api = create_app()
        api.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
        self.app = api.test_client()
        self.app.testing = True
        self.app.debug = True
        username='nedstark@doe.john'
        password='password'
        self.headers = {'Authorization': 'Basic %s' % b64encode(bytes(username+':'+password))}

    def tearDown(self):
        """ Reinit data for the next test
        """
        reload(api.global_data_sets)

    def test_get_services(self):
    	""" Test getting all the Services
    	"""
    	result = self.app.get('/services/', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"services":', result.data.strip('\n'))

    def test_get_service(self):
        """ Test getting a specific Service
        """
        result = self.app.get('/services/5', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"service":', result.data.strip('\n'))

    def test_add_service(self):
        """ Test adding a new Service
        """
        self.headers['Content-Type'] = 'application/json'
        result = self.app.post('/services/add', data='{"parameters":{"tags":["web"],"image":"centos 7.0", \
                                                       "user_data":null,"backups":false,"name":"db01", \
                                                       "ssh_keys":null,"region":"nyc3","volumes":null, \
                                                       "ipv6":true,"private_networking":null,"size":"2GB"}, \
                                                       "name":"Test service"}', headers=self.headers)
        self.assertEqual(result.status_code, 201)
        self.assertIn('Test service', result.data.strip('\n'))

    def test_delete_service(self):
        """ Test deleting a Service
        """
        result = self.app.delete('/services/5', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"result":', result.data.strip('\n'))

    def test_get_service_by_id(self):
        """ Test getting a Service by service_id
        """
        service_dict = srv_c.get_service_by_id(3, api.global_data_sets.services)
        self.assertEqual(3,service_dict[0]['id'])

    def test_get_service_by_name(self):
        """ Test getting a Service by Service name
        """
        service_dict = srv_c.get_service_by_name('Fluttershy', api.global_data_sets.services)
        self.assertEqual('Fluttershy',service_dict[0]['name'])
