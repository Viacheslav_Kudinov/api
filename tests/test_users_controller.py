"""  Unittests of authentication module
"""
import unittest
from base64 import b64encode
from unittest import TestCase
import api.global_data_sets
from api.api import create_app
from api.controllers import usr_c

class TestUsers(unittest.TestCase):
    """ This it main test class.
    """
    def setUp(self):
        """ Prepare the test fixture
        """
        api = create_app()
        api.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
        self.app = api.test_client()
        self.app.testing = True
        self.app.debug = True
        username='nedstark@doe.john'
        password='password'
        self.headers = {'Authorization': 'Basic %s' % b64encode(bytes(username+':'+password))}

    def tearDown(self):
        """ Reinit data for nex test
        """
        reload(api.global_data_sets)

    def test_get_users(self):
    	""" Test getting all the Users
        """
    	result = self.app.get('/users/', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"users":', result.data.strip('\n'))

    def test_get_user(self):
        """ Test getting specific User
        """
        result = self.app.get('/users/1', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"user":', result.data.strip('\n'))

    def test_add_user(self):
        """ Test adding a new User
        """
        self.headers['Content-Type'] = 'application/json'
        result = self.app.post('/users/add', data='{"login":"test_user@doe.john", "password":"password"}',headers=self.headers)
        self.assertEqual(result.status_code, 201)
        self.assertIn('test_user@doe.john', result.data.strip('\n'))

    def test_delete_user(self):
        """ Test deleting specific User
        """
        result = self.app.delete('/users/2', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"result":', result.data.strip('\n'))

    def test_get_user_services(self):
        """ Test getting all the Services of specific User
        """
        result = self.app.get('/users/5/services/', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"user_services"', result.data.strip('\n'))

    def test_get_user_service(self):
        """ Test getting specific Service of specific User
        """
        result = self.app.get('/users/4/services/4', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"user_service"', result.data.strip('\n'))

    def test_assign_user_service(self):
        """ Test assigning specific Service to specific User
        """
        result = self.app.post('/users/3/services/7', headers=self.headers)
        self.assertEqual(result.status_code, 201)
        self.assertIn('{"result"', result.data.strip('\n'))

    def test_delete_user_service(self):
        """ Test deleting specific Service from specific User
        """
        result = self.app.delete('/users/4/services/4', headers=self.headers)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"result"', result.data.strip('\n'))

    def test_get_user_by_id(self):
        """ Test getting User by user_id from Users
        """
        user_dict = usr_c.get_user_by_id(5, api.global_data_sets.users)
        self.assertEqual(5, user_dict[0]['id'])

    def test_get_user_by_login(self):
        """ Test getting User by login from Users
        """
        user_dict = usr_c.get_user_by_login('jonsnow@doe.john', api.global_data_sets.users)
        self.assertEqual('jonsnow@doe.john', user_dict[0]['login'])

    def test_get_user_by_id_n_login(self):
        """ Test getting User by login and id from Users
        """
        user_dict = usr_c.get_user_by_id_n_login(6, 'jonsnow@doe.john', api.global_data_sets.users)
        self.assertEqual('jonsnow@doe.john', user_dict[0]['login'])
        self.assertEqual(6, user_dict[0]['id'])

    def test_get_users_services_by_user_id(self):
        """ Test getting all the Services of specific User by user_id
        """
        users_services_dict = usr_c.get_users_services_by_user_id(2, api.global_data_sets.users_services)
        self.assertEqual(2, users_services_dict[0]['user_id'])

    def test_get_users_services_by_service_id(self):
        """ Test getting all the Services of specific User by service_id
        """
        users_services_dict = usr_c.get_users_services_by_service_id(5, api.global_data_sets.users_services)
        self.assertEqual(5, users_services_dict[0]['service_id'])

    def test_get_users_services_by_user_id_n_service_id(self):
        """ Test getting all the Services of specific User by user_id and service_id
        """
        users_services_dict = usr_c.get_users_services_by_user_id_n_service_id(2,
                                                                               3, api.global_data_sets.users_services)
        self.assertEqual(2, users_services_dict[0]['user_id'])
        self.assertEqual(3, users_services_dict[0]['service_id'])
