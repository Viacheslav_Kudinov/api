"""  Unittests of api module
"""

import unittest
from base64 import b64encode
from unittest import TestCase
from api.api import create_app

class TestAPI(unittest.TestCase):
    """ This it main test class.
    """

    def setUp(self):
        """ Prepare the test fixture
        """
        api = create_app()
        api.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
        self.app = api.test_client()
        self.app.testing = True
        self.app.debug = True
        username='nedstark@doe.john'
        password='password'
        self.headers = {'Authorization': 'Basic %s' % b64encode(bytes(username+':'+password))}

    def test_show_root(self):
    	""" Test getting the Root path
    	"""
    	result = self.app.get('/')
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.data.strip('\n'), '{"message":"This it RESTful API!!!"}')

    def test_bad_request(self):
        """ Test "Bad request" error
        """
        self.headers['Content-Type'] = 'application/json'
        result = self.app.post('/users/add', data='{"non_login":"test_user@doe.john", "password":"password"}',headers=self.headers)
        self.assertEqual(result.status_code, 400)
        self.assertIn('Bad request', result.data.strip('\n'))

    def test_unauthorized(self):
        """ Test "Unauthorized access" error
        """
        username='wrong_user@doe.john' # automation
        password='password'
        self.headers = {'Authorization': 'Basic %s' % b64encode(bytes(username+':'+password))}
        result = self.app.get('/users/', headers=self.headers)
        self.assertEqual(result.status_code, 401)
        self.assertIn('Unauthorized access', result.data.strip('\n'))

    def test_forbidden(self):
        """ Test "Forbidden" error
        """
        username='jonsnow@doe.john' # automation
        password='password'
        self.headers = {'Authorization': 'Basic %s' % b64encode(bytes(username+':'+password))}
        result = self.app.delete('/users/2', headers=self.headers)
        self.assertEqual(result.status_code, 403)
        self.assertIn('Forbidden', result.data.strip('\n'))

    def test_not_found(self):
        """ Test "Not found" error
        """
        result = self.app.get('/roles/4', headers=self.headers)
        self.assertEqual(result.status_code, 404)
        self.assertIn('Not found', result.data.strip('\n'))

    def test_not_allowed(self):
        """ Test "Method Not Allowed" error
        """
        result = self.app.put('/roles/4', headers=self.headers)
        self.assertEqual(result.status_code, 405)
        self.assertIn('Method Not Allowed', result.data.strip('\n'))

    def test_conflict(self):
        """ Test "Conflict" error
        """
        self.headers['Content-Type'] = 'application/json'
        # service with id=2 is aigned to user with id=2
        result = self.app.post('/users/1/services/2', headers=self.headers)
        self.assertEqual(result.status_code, 409)
        self.assertIn('Conflict', result.data.strip('\n'))

    def test_unprocessable(self):
        """ Test "Unprocessable Entity" error
        """
        result = self.app.delete('/users/1', headers=self.headers)
        self.assertEqual(result.status_code, 422)
        self.assertIn('Unprocessable Entity', result.data.strip('\n'))
