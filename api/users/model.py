"""  Users models
"""
# Gets all User set
def get_users():
    """ This function creates initial set of Users
    """
    users = [
        {'id': 1, 'login': 'nedstark@doe.john',
         'password': '3ab2c26f8b4ac7f85ff9edc9d63ce9037a7648d449473a9087293f7a8238dd50'},
        {'id': 2, 'login': 'kaimelannister@doe.john',
         'password': 'b68c937b4778dba803bc45da3d1536765bffad64c01bcf179521f7808bc52e0e'},
        {'id': 3, 'login': 'cerseilannister@doe.john',
         'password': '739225a4f71305b9c66a5c2754e163f15a720aaed9559d9d3f50ec3b4afcae5c'},
        {'id': 4, 'login': 'daenerystargaryen@doe.john',
         'password': '50336b8a36bc30e98b8b89115c835a512354634eec05bf4cf1231f2c6e646fc9'},
        {'id': 5, 'login': 'littlefinger@doe.john',
         'password': 'bc2f338453d0135ac4aac7c56e871f07d43c89051f310df439109d2e9bc7a7cd'},
        {'id': 6, 'login': 'jonsnow@doe.john',
         'password': '9aaa107a9cc4872455d961f2f746f55a6649339357c94a357089e9208879e7b1'}
    ]
    return users

# Get json schema for Users
def get_users_schema():
    """ This function set Users shema definition
    """
    users_schema = {
        'type' : 'object',
        'properties' : {
            'id': {'type': 'number'},
            'login': {'type': 'string',
                      'pattern': r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'},
            'password': {'type': 'string', 'minLength': 8}
        },
        'required': ['login', 'password']
    }
    return users_schema

# Gets all links User to Roles
def get_users_roles():
    """ This function creates initial set of Users
    """
    users_roles = [
        {'user_id': 1, 'role_id': 1},
        {'user_id': 2, 'role_id': 1},
        {'user_id': 3, 'role_id': 2},
        {'user_id': 4, 'role_id': 2},
        {'user_id': 5, 'role_id': 2},
        {'user_id': 6, 'role_id': 3}
    ]
    return users_roles

# Gets all links User to Services
def get_users_services():
    """ This function gets initial set of Users
    """
    users_services = [
        {'user_id': 1, 'service_id': 1},
        {'user_id': 2, 'service_id': 2},
        {'user_id': 2, 'service_id': 3},
        {'user_id': 4, 'service_id': 4},
        {'user_id': 5, 'service_id': 5},
    ]
    return users_services
