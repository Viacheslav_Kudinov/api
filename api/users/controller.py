""" This is Users api module
"""
import jsonschema #import validate, ValidationError, SchemaError
from flask import Blueprint, jsonify, abort, request
import api.authentication.auth_utils as auth_utils
from api.authentication.authentication import auth
import api.global_data_sets as gds
import api.controllers

# Create api_user blueprint
api_users = Blueprint('api_users', __name__)

# Get User by User_id
def get_user_by_id(user_id, users):
    """ Get User dict from Users by user_id
    """
    user_dict = [user for user in users if user['id'] == user_id]
    return user_dict

# Get User by User login
def get_user_by_login(login, users):
    """ Get User dict from Users by login
    """
    user_dict = [user for user in users if user['login'] == login]
    return user_dict

def get_user_by_id_n_login(user_id, login, users):
    """ Get User dict from Users by user_id
    """
    user_dict = [user for user in users
                 if user['id'] == user_id and user['login'] == login]
    return user_dict

# Get all the Services of specific User
def get_users_services_by_user_id(user_id, users_services):
    """ Get all the Services of specific User by user_id
    """
    usr_srv_dict = [usr_srv for usr_srv in users_services if usr_srv['user_id'] == user_id]
    return usr_srv_dict

def get_users_services_by_service_id(service_id, users_services):
    """ Get all the Services of specific User by service_id
    """
    usr_srv_dict = [usr_srv for usr_srv in users_services if usr_srv['service_id'] == service_id]
    return usr_srv_dict

def get_users_services_by_user_id_n_service_id(user_id, service_id, users_services):
    """ Get all the Services of specific User by user_id
    """
    usr_srv_dict = [usr_srv for usr_srv in users_services
                    if usr_srv['user_id'] == user_id and usr_srv['service_id'] == service_id]
    return usr_srv_dict

# Get all Users route
@api_users.route('/users/', methods=['GET'])
@auth.login_required
@auth_utils.requires_roles('admin')
def get_users():
    """ This function lists all available/active Users
    """
    return jsonify({'users': gds.users})

# Get specific User route
@api_users.route('/users/<int:user_id>', methods=['GET'])
@auth.login_required
@auth_utils.requires_roles('admin')
def get_user(user_id):
    """ This function lists information about specific User
    """
    #user_dict = [user for user in gds.users if user['id'] == user_id]
    user_dict = get_user_by_id(user_id, gds.users)
    if not user_dict:
        abort(404)
    return jsonify({'user': user_dict[0]})

# Add new User route
@api_users.route('/users/add', methods=['POST'])
@auth.login_required
@auth_utils.requires_roles('admin')
def add_user():
    """ This function creates a new User
    """
    try:
        jsonschema.validate(request.json, gds.users_schema)
    except jsonschema.ValidationError:
        abort(400)
    if not request.json or not 'login' in request.json:
        abort(400)
    user_dict = get_user_by_login(request.json['login'], gds.users)
    if user_dict:
        abort(409)
    next_user_id = gds.users[-1]['id'] + 1
    user = {
        'id': next_user_id,
        'login': request.json['login'],
        'password': auth_utils.hash_password(request.json['login'], request.json['password'])
    }
    gds.users.append(user)
    return jsonify({'user': gds.users[next_user_id-1]}), 201

# Delete specific User route
@api_users.route('/users/<int:user_id>', methods=['DELETE'])
@auth.login_required
@auth_utils.requires_roles('admin') #Only admin can delete User
def delete_user(user_id):
    """ This function deletes a Service
    """
    # preventing to delete myself
    tmp_user_dict = get_user_by_id_n_login(user_id, auth.username(), gds.users)
    if tmp_user_dict:
        abort(422)
    user_dict = get_user_by_id(user_id, gds.users)
    if not user_dict:
        abort(404)
    gds.users.remove(user_dict[0]) # remove User fron Users
    usr_srv_dict = get_users_services_by_user_id(user_id, gds.users_services)
    if usr_srv_dict:
        for usr_srv in usr_srv_dict:
            gds.users_services.remove(usr_srv) # revome link User to Services
    return jsonify({'result': True})

# Get all the Services of specific User route
@api_users.route('/users/<int:user_id>/services/', methods=['GET'])
@auth.login_required
@auth_utils.requires_roles('admin')
def get_user_services(user_id):
    """ This function lists all the services of specific User
    """
    tmp_services = []
    usr_srv_dict = get_users_services_by_user_id(user_id, gds.users_services)
    if not usr_srv_dict:
        abort(404)
    for usr_srv in usr_srv_dict:
        service_id = usr_srv['service_id']
        srv_dict = api.controllers.srv_c.get_service_by_id(service_id, gds.services)
        tmp_services.append(srv_dict[0])
    return jsonify({'user_services': tmp_services})

# Get specific Services of specific User route
@api_users.route('/users/<int:user_id>/services/<int:service_id>', methods=['GET'])
@auth.login_required
@auth_utils.requires_roles('admin')
def get_user_service(user_id, service_id):
    """ This function lists specific Services of specific User route
    """
    tmp_services = []
    usr_srv_dict = get_users_services_by_user_id_n_service_id(user_id,
                                                              service_id, gds.users_services)
    if not usr_srv_dict:
        abort(404)
    for usr_srv in usr_srv_dict:
        service_id = usr_srv['service_id']
        srv_dict = api.controllers.srv_c.get_service_by_id(service_id, gds.services)
        tmp_services.append(srv_dict[0])
    return jsonify({'user_service': tmp_services})

# Assign specific Services of specific User route
@api_users.route('/users/<int:user_id>/services/<int:service_id>', methods=['POST'])
@auth.login_required
@auth_utils.requires_roles('admin', 'user')
def assign_user_service(user_id, service_id):
    """ This function assigns specific Services of specific User route
    """
    usr_srv_dict = get_users_services_by_service_id(service_id, gds.users_services)
    if usr_srv_dict:
        abort(409)
    user_service = {
        'user_id': user_id,
        'service_id': service_id
    }
    gds.users_services.append(user_service)
    return jsonify({'result': True}), 201

# Delete specific Services of specific User route
@api_users.route('/users/<int:user_id>/services/<int:service_id>', methods=['DELETE'])
@auth.login_required
@auth_utils.requires_roles('admin', 'automation')
def delete_user_service(user_id, service_id):
    """ This function deletes specific Services of specific User route
    """
    usr_srv_dict = get_users_services_by_user_id_n_service_id(user_id,
                                                              service_id, gds.users_services)
    if not usr_srv_dict:
        abort(404)
    gds.users_services.remove(usr_srv_dict[0])
    return jsonify({'result': True})
