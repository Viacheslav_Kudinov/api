"""  Roles models
"""
def get_roles():
    """ This function creates initial set of Roles
    """
    roles = [
        {'id': 1, 'title': 'admin', 'description': 'Can Create/Delete Users and Services'},
        {'id': 2, 'title': 'user', 'description': 'Can Create Services'},
        {'id': 3, 'title': 'automation', 'description': 'Can delete Services'}
    ]
    return roles

def get_roles_schema():
    """ This function set Roles shema definition
    """
    roles_schema = {
        'type' : 'object',
        'properties' : {
            'id': {'type': 'number'},
            'title': {'type': 'string', 'minLength': 2},
            'description': {'type': 'string'}
        },
        'required': ['title', 'description']
    }
    return roles_schema
