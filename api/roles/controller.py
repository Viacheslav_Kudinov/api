""" This is Roles api module
"""
import jsonschema
from flask import Blueprint, jsonify, abort, request
import api.authentication.auth_utils as auth_utils
from api.authentication.authentication import auth
import api.global_data_sets as gds

# Create api_roles blueprint
api_roles = Blueprint('api_roles', __name__)

# Get role id
def get_role_by_id(role_id, roles):
    """ Get Role dict from Roles by role_id
    """
    role_dict = [role for role in roles if role['id'] == role_id]
    return role_dict

# Get role by role title
def get_role_by_title(role_title, roles):
    """ Get Role dict from Roles by role title
    """
    role_dict = [role for role in roles if role['title'] == role_title]
    return role_dict

# List all Roles
@api_roles.route('/roles/', methods=['GET'])
@auth.login_required
@auth_utils.requires_roles('admin') # Admin only
def get_roles():
    """ This function lists all available/active Roles
    """
    return jsonify({'roles': gds.roles})

# List specific Role
@api_roles.route('/roles/<int:role_id>', methods=['GET'])
@auth.login_required
@auth_utils.requires_roles('admin')  # Admin only
def get_role(role_id):
    """ This function lists information about specific Role
    """
    role_dict = get_role_by_id(role_id, gds.roles)
    if not role_dict:
        abort(404)
    return jsonify({'role': role_dict[0]})

# Create a new Role
@api_roles.route('/roles/add', methods=['POST'])
@auth.login_required
@auth_utils.requires_roles('admin') # Admin only
def add_role():
    """ This function creates a new Role
    """
    try:
        jsonschema.validate(request.json, gds.roles_schema)
    except jsonschema.ValidationError:
        abort(400)
    if not request.json or not 'title' in request.json:
        abort(400)
    role_dict = get_role_by_title(request.json['title'], gds.roles)
    if role_dict:
        abort(409)
    next_role_id = gds.roles[-1]['id'] + 1
    role = {
        'id': next_role_id,
        'title': request.json['title'],
        'description': request.json.get('description', '')
    }
    gds.roles.append(role)
    return jsonify({'role': gds.roles[next_role_id-1]}), 201
