""" Global data set
"""
from models import usr_m, rl_m, srv_m

# Get data sets
users = usr_m.get_users()
users_services = usr_m.get_users_services()
users_schema = usr_m.get_users_schema()
users_roles = usr_m.get_users_roles()

roles = rl_m.get_roles()
roles_schema = rl_m.get_roles_schema()

services = srv_m.get_services()
services_schema = srv_m.get_services_schema()
