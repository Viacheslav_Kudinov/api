""" This is simple RESTful API
"""
from flask import Flask, jsonify, abort, make_response, request
from controllers import usr_c, rl_c, srv_c
import global_data_sets as gds

def create_app():
    """ Flask application factory """
    app = Flask(__name__)
    app.config['JSON_SORT_KEYS'] = False
    #app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

    # Create one public endpoint
    @app.route('/', methods=['GET'])
    def show_root():
        """ The public endpoint
        """
        return make_response(jsonify({'message': 'This it RESTful API!!!'}))

    # Register Users api
    app.register_blueprint(usr_c.api_users)

    # Register Roles api
    app.register_blueprint(rl_c.api_roles)

    # Register Services api
    app.register_blueprint(srv_c.api_services)

    # Bad Request
    @app.errorhandler(400)
    def bad_request(error):
        """ This function makes response 400 - Bad Request
        """
        return make_response(jsonify({'error': 'Bad request'}), 400)

    # Unauthorized access
    @app.errorhandler(401)
    def unauthorized(error):
        """ This function makes response 401 - Unauthorized access
        """
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)

    # Forbidden
    @app.errorhandler(403)
    def forbidden(error):
        """ This function makes response 403 - Forbidden
        """
        return make_response(jsonify({'error': 'Forbidden'}), 403)

    # Not Found
    @app.errorhandler(404)
    def not_found(error):
        """ This function makes response 404 - Not Found
        """
        return make_response(jsonify({'error': 'Not found'}), 404)

    # Method Not Allowed
    @app.errorhandler(405)
    def not_allowed(error):
        """ This function makes response 405 - Method Not Allowed
        """
        return make_response(jsonify({'error': 'Method Not Allowed'}), 405)

    # Conflict
    @app.errorhandler(409)
    def conflict(error):
        """ This function makes response 409 - Conflict
        """
        return make_response(jsonify({'error': 'Conflict'}), 409)

    # Unprocessable Entity
    @app.errorhandler(422)
    def unprocessable(error):
        """ This function makes response 422 - Unprocessable Entity
        """
        return make_response(jsonify({'error': 'Unprocessable Entity'}), 422)

    return app

#if __name__ == '__main__':
#    application = create_app()
#    application.run()
