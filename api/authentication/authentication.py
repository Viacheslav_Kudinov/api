""" Authentication module
"""
from flask import abort, make_response, jsonify
from flask_httpauth import HTTPBasicAuth
import api.global_data_sets as gds
import api.authentication.auth_utils

auth = HTTPBasicAuth()

# This is a callback function
@auth.get_password
def get_password(username):
    """ This function returns the password associated with the username
    """
    try:
        user_dict = next(user for user in gds.users if user['login'] == username)
    except StopIteration:
        abort(401)
    if user_dict:
        return user_dict.get('password')
    return None

# Additional calback function for asswords are stored hashed
@auth.hash_password
def hash_pw(username, password):
    """ This function hashs the password associated with the username
    """
    return api.authentication.auth_utils.hash_password(username, password)

# Error handler
@auth.error_handler
def unauthorized():
    """ This function makes response 401 - Unauthorized
    """
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)
