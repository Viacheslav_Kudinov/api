""" Additional usres functionality modules
"""
import hashlib
from functools import wraps
from flask import abort
import api.global_data_sets as gds
import api.authentication.authentication as auth

# Getting a Role for a User
def get_user_role(username):
    """ This function returns current user's Role
    """
    user_dict = next(user for user in gds.users if user['login'] == username)
    if user_dict:
        user_id = user_dict.get('id')
        user_role_dict = next(ur for ur in gds.users_roles if ur['user_id'] == user_id)
        if user_role_dict:
            role_id = user_role_dict.get('role_id')
            role_dict = next(role for role in gds.roles if role['id'] == role_id)
            if role_dict:
                role_title = role_dict.get('title')
                return role_title
    return None

# Roles decorator
def requires_roles(*roles):
    """ This function defines required role for an endpoint
    """
    def decorator(fun):
        """ This is a decorator
        """
        @wraps(fun)
        def decorated_function(*args, **kwargs):
            """ This is decorated function
            """
            if get_user_role(auth.auth.username()) not in roles:
                abort(403)
            return fun(*args, **kwargs)
        return decorated_function
    return decorator

# Get hash for password
def hash_password(username, password):
    """ This function hashs the password associated with the username
    """
    return hashlib.sha256(password+username).hexdigest()
