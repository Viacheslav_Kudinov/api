""" This is Roles api module
"""
import jsonschema
from flask import Blueprint, jsonify, abort, request
import api.authentication.auth_utils as auth_utils
from api.authentication.authentication import auth
import api.global_data_sets as gds

# Create api_Services blueprint
api_services = Blueprint('api_services', __name__)

# Get Service by id
def get_service_by_id(service_id, services):
    """ Get Service dict from Services by service_id
    """
    service_dict = [service for service in services if service['id'] == service_id]
    return service_dict

# Get Service by Service name
def get_service_by_name(service_name, services):
    """ Get Service dict from Service by Service name
    """
    service_dict = [srvs for srvs in services if srvs['name'] == service_name]
    return service_dict

# Get all the services route
@api_services.route('/services/', methods=['GET'])
@auth.login_required # All authorized users can get all services
def get_services():
    """ This function lists all available/active Services
    """
    return jsonify({'services': gds.services})

# Get specific Service route
@api_services.route('/services/<int:service_id>', methods=['GET'])
@auth.login_required # All authorized users can get all services
def get_service(service_id):
    """ This function lists information about specific Service """
    service_dict = get_service_by_id(service_id, gds.services)
    if not service_dict:
        abort(404)
    return jsonify({'service': service_dict[0]})

# Add a new Service
@api_services.route('/services/add', methods=['POST'])
@auth.login_required
@auth_utils.requires_roles('admin') # Only admin can add new Service
def add_service():
    """ This function creates a new Service
    """
    try:
        jsonschema.validate(request.json, gds.services_schema)
    except jsonschema.ValidationError:
        abort(400)
    srv_dict = get_service_by_name(request.json['name'], gds.services)
    if srv_dict:
        abort(409)
    next_srv_id = gds.services[-1]['id'] + 1
    service = {'id': next_srv_id,
               'name': request.json['name'],
               'parameters': request.json['parameters']}
    gds.services.append(service)
    return jsonify({'service': gds.services[next_srv_id-1]}), 201

# Delete specific Service route
@api_services.route('/services/<int:service_id>', methods=['DELETE'])
@auth.login_required
@auth_utils.requires_roles('admin') #Only admin can delete Service
def delete_service(service_id):
    """ This function deletes a Service
    """
    service_dict = get_service_by_id(service_id, gds.services)
    if not service_dict:
        abort(404)
    gds.services.remove(service_dict[0])
    usr_srv_dict = [usr_srv for usr_srv in gds.users_services
                    if usr_srv['service_id'] == service_id]
    if usr_srv_dict:
        # Not array because of "A Service should only belong to one user at a time"
        gds.users_services.remove(usr_srv_dict[0])
    return jsonify({'result': True})
