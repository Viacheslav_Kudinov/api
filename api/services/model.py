"""  Services models
"""
def get_services():
    """ This function creates initial set of Services
    """
    services = [
        {'id': 1,
         'name': 'applejack',
         'parameters': {'name': 'site1.com', 'region': 'nyc3', 'size': '512mb',
                        'image': 'ubuntu-14-04-x64', 'ssh_keys': None, 'backups': False,
                        'ipv6': False, 'user_data': None, 'private_networking': None,
                        'volumes': None, 'tags': ['frontend']}
        },
        {'id': 2,
         'name': 'Fluttershy',
         'parameters': {'name': 'example.com', 'region': 'nyc3', 'size': '512mb',
                        'image': 'ubuntu-16-04-x64', 'ssh_keys': None, 'backups': False,
                        'ipv6': True, 'user_data': None, 'private_networking': None,
                        'volumes': None, 'tags': ['frontend']}
        },
        {'id': 3,
         'name': 'Rainbow Dash',
         'parameters': {'name': 'backend01', 'region': 'nyc3', 'size': '512mb',
                        'image': 'centos 7.0', 'ssh_keys': None, 'backups': False,
                        'ipv6': True, 'user_data': None, 'private_networking': None,
                        'volumes': None, 'tags': ['backend']}
        },
        {'id': 4,
         'name': 'Rarity',
         'parameters': {'name': 'backend02', 'region': 'nyc3', 'size': '1GB',
                        'image': 'centos 7.0', 'ssh_keys': None, 'backups': False,
                        'ipv6': False, 'user_data': None, 'private_networking': None,
                        'volumes': None, 'tags': ['web']}
        },
        {'id': 5,
         'name': 'Pinkie Pie',
         'parameters': {'name': 'db01', 'region': 'nyc3', 'size': '2GB',
                        'image': 'centos 7.0', 'ssh_keys': None, 'backups': False,
                        'ipv6': True, 'user_data': None, 'private_networking': None,
                        'volumes': None, 'tags': ['web']}
        }
    ]
    return services

def get_services_schema():
    """ This function set Services shema definition
    """
    services_schema = {
        'type' : 'object',
        'properties' : {
            'id': {'type': 'number'},
            'name': {'type': 'string'},
            'parameters': {
                'type': 'object',
                'properties' : {
                    'name': {'type': 'string'},
                    'region': {'type': 'string'},
                    'size': {'type': 'string'},
                    'image': {'type': 'string'},
                    'ssh_keys': {
                        'oneOf': [
                            {'type': 'string'},
                            {'type': 'null'}
                        ]
                    },
                    'backups': {'type': 'boolean'},
                    'ipv6': {'type': 'boolean'},
                    'user_data': {
                        'oneOf': [
                            {'type': 'string'},
                            {'type': 'null'}
                        ]
                    },
                    'private_networking': {
                        'oneOf': [
                            {'type': 'string'},
                            {'type': 'null'}
                        ]
                    },
                    'volumes': {
                        'oneOf': [
                            {'type': 'string'},
                            {'type': 'null'}
                        ]
                    },
                    'tags': {
                        'type': 'array',
                        'items': {
                            'type': 'string'
                        },
                        'minItems': 1,
                        'uniqueItems': True
                    }
                }
            }
        },
        'required': ['name', 'parameters']
    }
    return services_schema
