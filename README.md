A simple RESTful API
=========================

This repository includes the API project that implements for Backbase company as a part of an assignment.

The API implements a "Users, Services and Roles" system.

Requirements
------------

To install and run this application you need:

- Python 2.7
- virtualenv

Installation
------------

Thr installation process of the application and its dependencies:

    $ git clone https://Viacheslav_Kudinov@bitbucket.org/Viacheslav_Kudinov/api.git
    $ cd api
    $ virtualenv venv
    $ source venv/bin/activate
    (venv) pip install -r requirements.txt

The core dependencies are Flask, Flask-HTTPAuth, Flask-Script.


Run API server
--------------

Run on localhost only

    (venv) $ python manager.py runserver

or

    (venv) $ python manager.py runserver --host 127.0.0.1

Output:

     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)

Run on 0.0.0.0

    (venv) $ python manager.py runserver --host 0.0.0.0

Output:

     * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)

Run Unit Tests
--------------

To ensure that the installation is successful run the unit tests:

    (venv) $ python manager.py test

Output:

    Test "Bad request" error ... ok
    Test "Conflict" error ... ok
    Test "Forbidden" error ... ok
    Test "Method Not Allowed" error ... ok
    Test "Not found" error ... ok
    Test getting root path ... ok
    Test "Unauthorized access" error ... ok
    Test "Unprocessable Entity" error ... ok
    Test getting role for admin user ... ok
    Test getting hash of password for user ... ok
    Test of requires_roles ... SKIP: The requires_roles action has not been implemented
    Test getting of password (callback function) ... ok
    Test getting hash of user password (callback function) ... ok
    Test unauthorized mode for secured URL ... ok
    Test add_role path ... ok
    Test get_role path ... ok
    Test get_roles path ... ok
    Test add_service path ... ok
    Test delete_user path ... ok
    Test get_service path ... ok
    Test get_users path ... ok
    Test add_user path ... ok
    Test get_user_service path ... ok
    Test delete_user path ... ok
    Test delete_user_service path ... ok
    Test get_user path ... ok
    Test get_user_service path ... ok
    Test get_user_services path ... ok
    Test get_users path ... ok

    Name                                   Stmts   Miss Branch BrPart  Cover
    ------------------------------------------------------------------------
    api/__init__.py                            0      0      0      0   100%
    api/api.py                                29      2      2      1    90%
    api/authentication/__init__.py             0      0      0      0   100%
    api/authentication/auth_utils.py          27      1     14      6    83%
    api/authentication/authentication.py      17      1      4      1    90%
    api/controllers.py                         3      0      0      0   100%
    api/global_data_sets.py                    9      0      0      0   100%
    api/models.py                              3      0      0      0   100%
    api/roles/__init__.py                      0      0      0      0   100%
    api/roles/controller.py                   33      4      6      2    85%
    api/roles/model.py                         6      0      0      0   100%
    api/services/__init__.py                   0      0      0      0   100%
    api/services/controller.py                40      5      8      4    81%
    api/services/model.py                      6      0      0      0   100%
    api/users/__init__.py                      0      0      0      0   100%
    api/users/controller.py                   89      7     26      8    87%
    api/users/model.py                        12      0      0      0   100%
    ------------------------------------------------------------------------
    TOTAL                                    274     20     60     22    87%
    ----------------------------------------------------------------------
    Ran 29 tests in 0.160s

    OK (SKIP=1)

Run Pylint
----------

    (venv) $ python manager.py pylint

Output:

    No config file found, using default configuration
    ************* Module api.api
    E:  4, 0: Unable to import 'flask' (import-error)
    W:  5, 0: Relative import 'controllers', should be 'api.controllers' (relative-import)
    W:  6, 0: Relative import 'global_data_sets', should be 'api.global_data_sets' (relative-import)
    W: 32,20: Unused argument 'error' (unused-argument)
    W: 39,21: Unused argument 'error' (unused-argument)
    W: 46,18: Unused argument 'error' (unused-argument)
    W: 53,18: Unused argument 'error' (unused-argument)
    W: 60,20: Unused argument 'error' (unused-argument)
    W: 67,17: Unused argument 'error' (unused-argument)
    W: 74,22: Unused argument 'error' (unused-argument)
    W: 39, 4: Unused variable 'unauthorized' (unused-variable)
    W: 16, 4: Unused variable 'show_root' (unused-variable)
    W: 46, 4: Unused variable 'forbidden' (unused-variable)
    W: 32, 4: Unused variable 'bad_request' (unused-variable)
    W: 60, 4: Unused variable 'not_allowed' (unused-variable)
    W: 53, 4: Unused variable 'not_found' (unused-variable)
    W: 74, 4: Unused variable 'unprocessable' (unused-variable)
    W: 67, 4: Unused variable 'conflict' (unused-variable)
    W:  4, 0: Unused abort imported from flask (unused-import)
    W:  4, 0: Unused request imported from flask (unused-import)
    W:  6, 0: Unused global_data_sets imported as gds (unused-import)
    ************* Module api.models
    W:  4, 0: Relative import 'users.model', should be 'api.users.model' (relative-import)
    W:  5, 0: Relative import 'roles.model', should be 'api.roles.model' (relative-import)
    W:  6, 0: Relative import 'services.model', should be 'api.services.model' (relative-import)
    W:  4, 0: Unused users.model imported as usr_m (unused-import)
    W:  5, 0: Unused roles.model imported as rl_m (unused-import)
    W:  6, 0: Unused services.model imported as srv_m (unused-import)
    ************* Module api.global_data_sets
    W:  4, 0: Relative import 'models', should be 'api.models' (relative-import)
    ************* Module api.controllers
    W:  4, 0: Relative import 'users.controller', should be 'api.users.controller' (relative-import)
    W:  5, 0: Relative import 'roles.controller', should be 'api.roles.controller' (relative-import)
    W:  6, 0: Relative import 'services.controller', should be 'api.services.controller' (relative-import)
    W:  4, 0: Unused users.controller imported as usr_c (unused-import)
    W:  5, 0: Unused roles.controller imported as rl_c (unused-import)
    W:  6, 0: Unused services.controller imported as srv_c (unused-import)
    ************* Module api.roles.controller
    E:  4, 0: Unable to import 'jsonschema' (import-error)
    E:  5, 0: Unable to import 'flask' (import-error)
    ************* Module api.services.controller
    E:  4, 0: Unable to import 'jsonschema' (import-error)
    E:  5, 0: Unable to import 'flask' (import-error)
    ************* Module api.authentication.authentication
    E:  4, 0: Unable to import 'flask' (import-error)
    E:  5, 0: Unable to import 'flask_httpauth' (import-error)
    ************* Module api.authentication.auth_utils
    E:  6, 0: Unable to import 'flask' (import-error)
    ************* Module api.users.controller
    E:  4, 0: Unable to import 'jsonschema' (import-error)
    E:  5, 0: Unable to import 'flask' (import-error)
    ************* Module api.users.__init__
    R:  1, 0: Cyclic import (api.authentication.auth_utils -> api.authentication.authentication) (cyclic-import)
    R:  1, 0: Similar lines in 3 files
    ==api.roles.controller:3
    ==api.services.controller:3
    ==api.users.controller:3
    import jsonschema
    from flask import Blueprint, jsonify, abort, request
    import api.authentication.auth_utils as auth_utils
    from api.authentication.authentication import auth
    import api.global_data_sets as gds

    # Create api_Services blueprint (duplicate-code)

    Report
    ======
    246 statements analysed.

    Statistics by type
    ------------------

    +---------+-------+-----------+-----------+------------+---------+
    |type     |number |old number |difference |%documented |%badname |
    +=========+=======+===========+===========+============+=========+
    |module   |17     |17         |=          |100.00      |0.00     |
    +---------+-------+-----------+-----------+------------+---------+
    |class    |0      |0          |=          |0           |0        |
    +---------+-------+-----------+-----------+------------+---------+
    |method   |0      |0          |=          |0           |0        |
    +---------+-------+-----------+-----------+------------+---------+
    |function |40     |40         |=          |100.00      |0.00     |
    +---------+-------+-----------+-----------+------------+---------+

    External dependencies
    ---------------------
    ::

        api
          \-authentication
          | \-auth_utils (api.authentication.authentication,api.roles.controller,api.services.controller,api.users.controller)
          | \-authentication (api.authentication.auth_utils,api.roles.controller,api.services.controller,api.users.controller)
          \-controllers (api.api)
          \-global_data_sets (api.api,api.authentication.auth_utils,api.authentication.authentication,api.roles.controller,api.services.controller,api.users.controller)
          \-models (api.global_data_sets)
          \-roles
          | \-controller (api.controllers)
          | \-model (api.models)
          \-services
          | \-controller (api.controllers)
          | \-model (api.models)
          \-users
            \-controller (api.controllers)
            \-model (api.models)

    Raw metrics
    -----------

    +----------+-------+------+---------+-----------+
    |type      |number |%     |previous |difference |
    +==========+=======+======+=========+===========+
    |code      |379    |55.74 |379      |=          |
    +----------+-------+------+---------+-----------+
    |docstring |184    |27.06 |184      |=          |
    +----------+-------+------+---------+-----------+
    |comment   |46     |6.76  |46       |=          |
    +----------+-------+------+---------+-----------+
    |empty     |71     |10.44 |71       |=          |
    +----------+-------+------+---------+-----------+

    Duplication
    -----------

    +-------------------------+------+---------+-----------+
    |                         |now   |previous |difference |
    +=========================+======+=========+===========+
    |nb duplicated lines      |14    |14       |=          |
    +-------------------------+------+---------+-----------+
    |percent duplicated lines |2.112 |2.112    |=          |
    +-------------------------+------+---------+-----------+

    Messages by category
    --------------------

    +-----------+-------+---------+-----------+
    |type       |number |previous |difference |
    +===========+=======+=========+===========+
    |convention |0      |0        |=          |
    +-----------+-------+---------+-----------+
    |refactor   |2      |2        |=          |
    +-----------+-------+---------+-----------+
    |warning    |33     |33       |=          |
    +-----------+-------+---------+-----------+
    |error      |10     |10       |=          |
    +-----------+-------+---------+-----------+

    % errors / warnings by module
    -----------------------------

    +----------------------------------+------+--------+---------+-----------+
    |module                            |error |warning |refactor |convention |
    +==================================+======+========+=========+===========+
    |api.users.controller              |20.00 |0.00    |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.services.controller           |20.00 |0.00    |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.roles.controller              |20.00 |0.00    |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.authentication.authentication |20.00 |0.00    |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.api                           |10.00 |60.61   |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.authentication.auth_utils     |10.00 |0.00    |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.models                        |0.00  |18.18   |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.controllers                   |0.00  |18.18   |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.global_data_sets              |0.00  |3.03    |0.00     |0.00       |
    +----------------------------------+------+--------+---------+-----------+
    |api.users.__init__                |0.00  |0.00    |100.00   |0.00       |
    +----------------------------------+------+--------+---------+-----------+

    Messages
    --------

    +----------------+------------+
    |message id      |occurrences |
    +================+============+
    |import-error    |10          |
    +----------------+------------+
    |unused-import   |9           |
    +----------------+------------+
    |relative-import |9           |
    +----------------+------------+
    |unused-variable |8           |
    +----------------+------------+
    |unused-argument |7           |
    +----------------+------------+
    |duplicate-code  |1           |
    +----------------+------------+
    |cyclic-import   |1           |
    +----------------+------------+

    ------------------------------------------------------------------
    Your code has been rated at 6.54/10 (previous run: 6.54/10, +0.00)

Run API server in Docker
------------------------

Build the Docker image

    $ git clone https://Viacheslav_Kudinov@bitbucket.org/Viacheslav_Kudinov/api.git
    $ cd api
    $ docker build -t api .

Output:

    Sending build context to Docker daemon  141.8kB
    Step 1/7 : FROM python:2.7-alpine
     ---> 2251db12bd56
    Step 2/7 : WORKDIR /api
     ---> Using cache
     ---> 4a3641f775d3
    Step 3/7 : RUN apk add --update     python     python-dev     py-pip     build-base     libffi     libffi-dev     openssl     openssl-dev   && rm -rf /var/cache/apk/*   && pip install --upgrade pip   && pip install virtualenv
     ---> Using cache
     ---> 4804006fe6b4
    Step 4/7 : COPY . /api
     ---> 195dbfa7e7a4
    Step 5/7 : RUN virtualenv venv && venv/bin/pip install -r requirements.txt
     ---> Running in 8f7d1955638c
    New python executable in /api/venv/bin/python

    ...................

    Removing intermediate container 8f7d1955638c
    ---> c16675a087de
    Step 6/7 : EXPOSE 5000
    ---> Running in ecdfd77867a6
    Removing intermediate container ecdfd77867a6
    ---> 5cafec1b0e40
    Step 7/7 : CMD ["venv/bin/python","manager.py","runserver","--host","0.0.0.0"]
    ---> Running in 280adfa77e42
    Removing intermediate container 280adfa77e42
    ---> 5c0d49a85b3d
    Successfully built 5c0d49a85b3d
    Successfully tagged api:latest


Run a Docker container

    $ docker run -d -p 5000:5000 api

Output:

    db7e55e796fef1e0bcfbc2cdff084d05cae94a4c77b42f8804a5c545da619f17

Check that the Docker container is running

    $ docker ps

Output:

    CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
    db7e55e796fe        api                 "venv/bin/python man…"   40 seconds ago      Up 39 seconds       0.0.0.0:5000->5000/tcp   confident_poitras

Several examples of requests
---------------------------
SERVICES

    # Get all the Services
    $ curl -u jonsnow@doe.john:password -iL http://localhost:5000/services/

    # Get a Service with id=1
    $ curl -u jonsnow@doe.john:password -iL http://localhost:5000/services/1

    # Add a new Service
    $ curl -u nedstark@doe.john:password -i -H "Content-Type: application/json" -X POST -d '{"parameters":{"tags":["web"],"image":"centos 7.0","user_data":null,"backups":false,"name":"db01","ssh_keys":null,"region":"nyc3","volumes":null,"ipv6":true,"private_networking":null,"size":"2GB"},"name":"Test"}'  http://localhost:5000/services/add

    # Delete a Service
    $ curl -u nedstark@doe.john:password -X DELETE -iL http://localhost:5000/services/5

USERS

    # Get all the Users
    $ curl -u jonsnow@doe.john:password -iL http://localhost:5000/users/

    # Get specific User with id=1
    $ curl -u jonsnow@doe.john:password -iL http://localhost:5000/users/1

    # Add new User
    $ curl -u nedstark@doe.john:password -i -H "Content-Type: application/json" -X POST -d '{"login":"test__@doe.john", "password":"password"}'  http://localhost:5000/users/add

    # Delete User with id=2
    $ curl -u nedstark@doe.john:password -X DELETE -iL http://localhost:5000/users/2

    # Get all the Service of specific User with id=5
    $ curl -u nedstark@doe.john:password -iL http://localhost:5000/users/5/services/

    # Get specific Service of specific User with id=5 and Service id=5
    $ curl -u nedstark@doe.john:password -iL http://localhost:5000/users/5/services/5

    # Assign specific Services to specific User
    $ curl -u nedstark@doe.john:password -X POST -iL http://localhost:5000/users/3/services/3

    # Delete specific Services from specific User
    $ curl -u nedstark@doe.john:password -X DELETE -iL http://localhost:5000/users/3/services/3

ROLES

    # Get all the Roles
    $ curl -u nedstark@doe.john:password -i   http://localhost:5000/roles/

    # Get specific Role with id=2
    $ curl -u nedstark@doe.john:password -i   http://localhost:5000/roles/2

    # Create a new Role
    $ curl -u nedstark@doe.john:password -i -H "Content-Type: application/json" -X POST -d '{"title":"PM", "description": "Can do nothing"}'  http://localhost:5000/roles/add
