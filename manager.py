#!/usr/bin/env python
from flask import Flask
from flask_script import Manager
from api.api import create_app

manager = Manager(create_app())

@manager.command
def test():
    from subprocess import call
    call(['nosetests', '-v',
          '--with-coverage', '--cover-package=api', '--cover-branches',
          '--cover-erase', '--cover-html', '--cover-html-dir=cover'])

@manager.command
def pylint():
    from subprocess import call
    call(['pylint', 'api',
          '--reports=y', '-d', 'not-callable', '--disable=invalid-name'])


if __name__ == '__main__':
    manager.run()
